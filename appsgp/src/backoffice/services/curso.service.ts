import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Curso } from "../entities/curso.entity";

@Injectable()
export class CursoService {
    constructor(
        @InjectRepository(Curso)
        private readonly repository: Repository<Curso>,
    ){}

    async get(): Promise<Curso[]> {
        return await this.repository.find();
    }

    async post(curso: Curso) {
        await this.repository.save(curso);
    }

    async put(id: number, curso: Curso) {
        await this.repository.update(id, curso);
    }

/* async delete(id: number) {
        await this.repository.delete(id);
    } */
}