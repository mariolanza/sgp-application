import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Disciplina } from "../entities/disciplina.entity";
import { Repository } from "typeorm";

@Injectable()
export class DisciplinaService {
    constructor(
        @InjectRepository(Disciplina)
        private readonly repository: Repository<Disciplina>,
    ){}

    async get(): Promise<Disciplina[]> {
        return await this.repository.find();
    }

    async post(disciplina: Disciplina) {
        await this.repository.save(disciplina);
    }

    async put(id: number, disciplina: Disciplina) {
        await this.repository.update(id, disciplina);
    }

    async delete(id: number) {
        await this.repository.delete(id);
    }
}