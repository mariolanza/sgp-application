import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Pagamento } from "../entities/pagamento.entity";

@Injectable()
export class PagamentoService {
    constructor(
        @InjectRepository(Pagamento)
        private readonly repository: Repository<Pagamento>,
    ){}

    async get(): Promise<Pagamento[]> {
        return await this.repository.find();
    }

    async post(pagamento: Pagamento) {
        await this.repository.save(pagamento);
    }

    async put(id: number, pagamento: Pagamento) {
        await this.repository.update(id, pagamento);
    }

  /*  async delete(id: number) {
        await this.repository.delete(id);
    } */
}