import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Turma } from "../entities/turma.entity";
import { Repository } from "typeorm";

@Injectable()
export class TurmaService {
    constructor(
        @InjectRepository(Turma)
        private readonly repository: Repository<Turma>,
    ){}

    async get(): Promise<Turma[]> {

        var result = await this.repository.find({ relations: ["curso"] });
        
        return result;

    }

    async post(turma: Turma) {
        await this.repository.save(turma);
    }

    async put(id: number, turma: Turma) {
        await this.repository.update(id, turma);
    }

    async delete(id: number) {
        await this.repository.delete(id);
    }
}