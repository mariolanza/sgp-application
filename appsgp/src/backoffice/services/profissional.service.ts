import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Profissional } from "../entities/profissional.entity";
import { Repository } from "typeorm";

@Injectable()
export class ProfissionalService {
    constructor(
        @InjectRepository(Profissional)
        private readonly repository: Repository<Profissional>,
    ){}

    async get(): Promise<Profissional[]> {
        return await this.repository.find();
    }

    async post(profissional: Profissional) {
        await this.repository.save(profissional);
    }

    async put(id: number, profissional: Profissional) {
        await this.repository.update(id, profissional);
    }

    async delete(id: number) {
        await this.repository.delete(id);
    }
}