import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Account } from "../entities/account.entity";


@Injectable()
export class AccountService {
    constructor(
        @InjectRepository(Account)
        private readonly repository: Repository<Account>,
    ){}

    async get(): Promise<Account[]> {
        return await this.repository.find();
    }

    async post(account: Account) {
        await this.repository.save(account);
    }

    async put(id: number, account: Account) {
        await this.repository.update(id, account);
    }

    async delete(id: number) {
        await this.repository.delete(id);
    }

    async authenticate(login, senha): Promise<Account>{
        return await this.repository.findOne({
            'login': login,
            'senha': senha
        })
    }

    async update(login: string, data: any){
        var user = await this.repository.findOne({login});
        console.log(user);

        var updatedUser = await this.repository.update(user.id, data);
        return updatedUser;
    }

}