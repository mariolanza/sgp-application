import { ProfissionalController } from './controllers/profissional.controller';
import { TurmaController } from './controllers/turma.controller';
import { CursoController } from './controllers/curso.controller';
import { DisciplinaController } from './controllers/disciplina.controller';
import { PagamentoController } from './controllers/pagamento.controller';
import { AccountController } from './controllers/account.controller';

import { ProfissionalService } from './services/profissional.service';
import { TurmaService } from './services/turma.service';
import { DisciplinaService } from './services/disciplina.service';
import { CursoService } from './services/curso.service';
import { PagamentoService } from './services/pagamento.service';
import { AccountService } from './services/account.service';
import { AuthService } from 'src/shared/services/auth.service';
import { JwtStrategy } from 'src/shared/strategies/jwt.strategy';

import { Profissional } from './entities/profissional.entity';
import { Turma } from './entities/turma.entity';
import { Curso } from './entities/curso.entity';
import { Disciplina } from './entities/disciplina.entity';
import { Account } from './entities/account.entity';
import { Pagamento } from './entities/pagamento.entity';

import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';


@Module({

    imports: [ 
        TypeOrmModule.forFeature([
            Profissional,
            Turma,
            Curso,
            Disciplina,
            Account,
            Pagamento,
        ]),

        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
            secretOrPrivateKey: '1abc2def',
            signOptions: {
                expiresIn: 3600,
            },
        }),
    ],

    controllers: [
        ProfissionalController,
        TurmaController,
        CursoController,
        DisciplinaController,
        PagamentoController,
        AccountController,
    ],
     
    providers: [
        ProfissionalService,
        TurmaService, 
        DisciplinaService,
        CursoService,
        PagamentoService,
        AccountService,
        AuthService,
        JwtStrategy
    ],

})
export class BackofficeModule {}
