import { Contract } from "./contract";
import { Flunt } from "../../utils/flunt";
import { Injectable } from "@nestjs/common";
import { Pagamento } from "../models/pagamento.model";

@Injectable()
export class CreatePagamentoContract implements Contract{
    errors: any[];    
    
    validate(model: Pagamento): boolean {
        const flunt = new Flunt();

        flunt.isRequired(model.data, 'Necessário inserir a data do pagamento!');
        flunt.isRequired(model.valor, 'Necessário inserir o valor das horas a ser pago!');
        flunt.isRequired(model.periodo, 'Necessário inserir o mês!');
        flunt.isRequired(model.profissional.nome, 'Necessário inserir o profissional!');
        flunt.isRequired(model.profissional.cargo, 'Necessário inserir o profissional!');   
        flunt.isRequired(model.horas_em_sala, 'Necessário inserir horas em sala!');
        flunt.isRequired(model.curso.descricao, 'Necessário inserir curso!');
        flunt.isRequired(model.disciplina.descricao, 'Necessário inserir a disciplina!');
        
        this.errors = flunt.errors;

        return flunt.isValid();

    }
}