import { Contract } from "./contract";
import { Flunt } from "../../utils/flunt";
import { Injectable } from "@nestjs/common";
import { Curso } from "../models/curso.model";

@Injectable()
export class CreateCursoContract implements Contract{
    errors: any[];    
    
    validate(model: Curso): boolean {
        const flunt = new Flunt();

        flunt.isRequired(model.descricao, 'Necessário inserir nome do curso!');
        flunt.isRequired(model.data_inicio, 'Necessário inserir data de início do curso!');
        
        this.errors = flunt.errors;

        return flunt.isValid();

    }
}