import { Contract } from "./contract";
import { Flunt } from "../../utils/flunt";
import { Injectable } from "@nestjs/common";
import { Account } from "../models/account.model";

@Injectable()
export class CreateAccountContract implements Contract{
    errors: any[];    
    
    validate(model: Account): boolean {
        const flunt = new Flunt();

        flunt.isRequired(model.login, 'Necessário inserir login!');
        flunt.isRequired(model.senha, 'Necessário inserir senha!');

        this.errors = flunt.errors;

        return flunt.isValid();

    }
}