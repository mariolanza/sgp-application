import { Contract } from "./contract";
import { Flunt } from "../../utils/flunt";
import { Injectable } from "@nestjs/common";
import { Disciplina } from "../models/disciplina.model";

@Injectable()
export class CreateDisciplinaContract implements Contract{
    errors: any[];    
    
    validate(model: Disciplina): boolean {
        const flunt = new Flunt();

        flunt.isRequired(model.descricao, 'Necessário inserir nome da disciplina!');
        flunt.isRequired(model.carga_horaria_disciplina, 'Necessário inserir carga horária da disciplina por mês!');

        this.errors = flunt.errors;

        return flunt.isValid();

    }
}