import { Contract } from "./contract";
import { Flunt } from "../../utils/flunt";
import { Injectable } from "@nestjs/common";
import { Turma } from "../models/turma.model";

@Injectable()
export class CreateTurmaContract implements Contract{
    errors: any[];    
    
    validate(model: Turma): boolean {
        const flunt = new Flunt();

        flunt.isRequired(model.codigo, 'Necessário inserir o código da Turma!');
        flunt.isRequired(model.curso, 'Necessário inserir curso!');

        this.errors = flunt.errors;

        return flunt.isValid();

    }
}