import { Contract } from "./contract";
import { Profissional } from "../models/profissional.model";
import { Flunt } from "../../utils/flunt";
import { Injectable } from "@nestjs/common";

@Injectable()
export class CreateProfissionalContract implements Contract{
    errors: any[];    
    
    validate(model: Profissional): boolean {
        const flunt = new Flunt();

        flunt.isRequired(model.nome, 'Necessário inserir nome!');
        flunt.isRequired(model.matricula, 'Necessário inserir matrícula!');
        flunt.isRequired(model.cargo, 'Necessário inserir cargo!');
        flunt.isFixedLen(model.cpf, 11, 'CPF inválido!')

        this.errors = flunt.errors;

        return flunt.isValid();

    }
}