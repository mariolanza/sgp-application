import { Entity, PrimaryGeneratedColumn, Column,  } from 'typeorm';

@Entity() 
export class Disciplina {
    @PrimaryGeneratedColumn()
    id: number;   

    @Column({length: 12})
    descricao: string;

    @Column('int')
    carga_horaria_disciplina: number;
}