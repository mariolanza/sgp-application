import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany,  } from 'typeorm';


@Entity() 
export class Account {
    @PrimaryGeneratedColumn()
    id: number;   

    @Column({length: 64})
    login: string;

    @Column({length: 32})
    senha: string; 
}