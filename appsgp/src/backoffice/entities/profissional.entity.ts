import { Entity, PrimaryGeneratedColumn, Column, OneToMany,  } from 'typeorm';
import { Account } from './account.entity';

@Entity() 
export class Profissional {
    
    @PrimaryGeneratedColumn()
    id: number;   

    @Column({length: 12})
    matricula: string;

    @Column('boolean')
    declaração: boolean;

    @Column({length: 12})
    rg: string;

    @Column({length: 12})
    orgao_expedidor: string;

    @Column({length: 2})
    uf: string;

    @Column({length: 11})
    cpf: string;

    @Column({length: 12})
    pis: string;

    @Column({length: 12})
    banco: string;

    @Column({length: 12})
    agencia: string;

    @Column({length: 12})
    conta: string;

    @Column({length: 128})
    endereco: string;

    @Column({length: 128})
    bairro: string;

    @Column({length: 128})
    cidade: string;

    @Column({length: 2})
    estado: string;

    @Column({length: 12})
    cep: string;

    @Column({length: 32})
    telefone: string;

    @Column('date')
    data_nascimento: Date;

    @Column({length: 64})
    cargo: string;

     @Column()
     titulacao: number;
}