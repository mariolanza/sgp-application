import { Entity, PrimaryGeneratedColumn, Column, ManyToOne,  } from 'typeorm';
import { Profissional } from './profissional.entity';
import { Curso } from './curso.entity';
import { Disciplina } from './disciplina.entity';


@Entity() 
export class Pagamento {
    @PrimaryGeneratedColumn()
    id: number;   

    @Column('date')
    data: Date;

    @Column('decimal')
    valor: number;

    @Column({length: 12})
    periodo: string;
     
    @Column()
    horas_em_sala: number;
    
    @Column('decimal')
    valor_total: number;

    @ManyToOne(() => Curso, (c) => c)
    curso: Curso; 

    @ManyToOne(() => Disciplina, (d) => d)
    disciplina: Disciplina; 

    @ManyToOne(() => Profissional, (p) => p)
    profissional: Profissional; 
}