import { Entity, PrimaryGeneratedColumn, Column, OneToMany,  } from 'typeorm';
import { Turma } from './turma.entity';

@Entity() 
export class Curso {
    @PrimaryGeneratedColumn()
    id: number;   

    @Column({length: 12})
    descricao: string;
    
    @Column('date')
    data_inicio: Date;

    @Column('int')
    ultima_parcela_paga_coordenador: number;

    @OneToMany(() => Turma, (t) => t)
    turma: Turma[]; 

}