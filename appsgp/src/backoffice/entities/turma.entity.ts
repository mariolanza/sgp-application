import { Entity, PrimaryGeneratedColumn, Column, ManyToOne,  } from 'typeorm';
import { Curso } from './curso.entity';


@Entity() 
export class Turma {
    @PrimaryGeneratedColumn()
    id: number;   

    @Column({length: 12})
    codigo: string;

    @ManyToOne(() => Curso, (c) => c)
    curso: Curso; 
}