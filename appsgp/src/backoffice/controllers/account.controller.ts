import { Controller, Get, Post, Put, Delete, Req, Param, Body, UseInterceptors, HttpException, HttpStatus, UseGuards } from "@nestjs/common";
import { Result } from "../models/result.model";
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";
import { AccountService } from "../services/account.service";
import { CreateAccountContract } from "../contracts/account.contract";
import { Account } from "../entities/account.entity";
import { AuthService } from "src/shared/services/auth.service";
import { JwtAuthGuard } from "src/shared/guards/auth.guard";
import { ResetPassword } from "../models/reset_password.model";
import { Guid } from "guid-typescript";
import { ChangePassword } from "../models/change_password.model";

// localhost:3000/account
@Controller('account')
export class AccountController {

    constructor(private readonly service: AccountService,
                private readonly authServie:  AuthService){}

    // localhost:3000/account/get
    //recebe todos as accounts e retorna a lista de accounts
    @Get('get')
    @UseGuards(JwtAuthGuard)
    findAll(@Req() request){
        console.log(request.login);
        return [];
    }
    async get() {
        try{
            const account = await this.service.get(); 
            return new Result(null, true, account, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível listar os usuários.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }

    //autenticar  - localhost:3000/account/authenticate
    @Post('authenticate')
    async authenticate(@Body() body: Account): Promise<any> {
        const user = await this.service.authenticate(body.login, body.senha);

        //caso não encontre o usuário
        if(!user)
        throw new HttpException(new Result('Usuário ou senha inválidos.', false, null, null), HttpStatus.UNAUTHORIZED)
        
        //gera o token
        const token = await this.authServie.createToken(user.login)
            return new Result(null, true, token, null);
    }

    @Post('reset-password')
    async resetPassword(@Body() body: ResetPassword): Promise<any> {
        try{
            const senha = Guid.create().toString().substring(0,8).replace('-', '')
            await this.service.update(body.login, {senha: senha})
            return new HttpException(new Result('Uma nova senha foi enviada para o seu e-mail.', true, null, null), HttpStatus.BAD_REQUEST)
        }
        catch(error){
            throw new HttpException(new Result('Não foi possível resturar a sua senha.', false, null, null), HttpStatus.BAD_REQUEST)
        }         
    }

    @Post('change-password')
    @UseGuards(JwtAuthGuard)
    async changePassword(@Param('id') id, @Req() request, @Body() body: ChangePassword): Promise<any> {
        try{ //encripta senha
            await this.service.update(request.body.login, {senha: body.nova_senha})
            return new HttpException(new Result('Senha alterada com sucesso.', true, null, null), HttpStatus.BAD_REQUEST)
        }
        catch(error){
            throw new HttpException(new Result('Não foi possível alterar a sua senha.', false, null, null), HttpStatus.BAD_REQUEST)
        }         
    }

    @Post('refresh')
    @UseGuards(JwtAuthGuard)
    async refreshToken(@Req() request): Promise<any>{
        //gera token
        const token = await this.authServie.createToken(request.user.login)
        return new Result(null, true, token, null);
    }

    @Post('new')
    @UseInterceptors(new ValidatorInterceptor(new CreateAccountContract))
    async post(@Body() body: Account) {
        try{
            await this.service.post(body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível inserir o usuário.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    } 
    
    @Put('edit')
    async put(@Param('id') id, @Body() body: Account) {
        try{
            await this.service.put(id, body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o usuário.', false, null, error), HttpStatus.BAD_REQUEST)
        } 
    }

    @Delete('delete')
    async delete(@Param('id') id) {
        try{
            await this.service.delete(id); 
            return new Result("Usuário removido com sucesso!", true, null, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o usuário.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }    
}