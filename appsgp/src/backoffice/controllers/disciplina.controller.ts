import { Controller, Get, Post, Put, Delete, Param, Body, UseInterceptors, HttpException, HttpStatus } from "@nestjs/common";
import { Result } from "../models/result.model";
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";
import { DisciplinaService } from "../services/disciplina.service";
import { CreateDisciplinaContract } from "../contracts/disciplina.contract";
import { Disciplina } from "../entities/disciplina.entity";

// localhost:3000/disciplina
@Controller('disciplina')
export class DisciplinaController {

    constructor(private readonly service: DisciplinaService){}

    @Get('get')
    async get() {
        try{
            const disciplinas = await this.service.get(); 
            return new Result(null, true, disciplinas, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível listar as disciplinas.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }

    @Get(':id')
    @UseInterceptors(new ValidatorInterceptor(new CreateDisciplinaContract))
    async getId(@Param('id') id, @Body() body: Disciplina) {
        try{
            await this.service.get(); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível buscar a disciplina.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }

    @Post('new')
    @UseInterceptors(new ValidatorInterceptor(new CreateDisciplinaContract))
    async post(@Body() body: Disciplina) {
        try{
            await this.service.post(body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível incluir a disciplina.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }
    
    @Put('edit')
    async put(@Param('id') id, @Body() body: Disciplina) {
        try{
            await this.service.put(id, body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar a disciplina.', false, null, error), HttpStatus.BAD_REQUEST)
        } 
    }

    @Delete('delete')
    async delete(@Param('id') id) {
        try{
            await this.service.delete(id); 
            return new Result("Disciplina removida com sucesso!", true, null, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar a disciplina.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }    
}