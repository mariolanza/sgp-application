import { Controller, Get, Post, Put, Delete, Param, Body, UseInterceptors, HttpException, HttpStatus } from "@nestjs/common";
import { Result } from "../models/result.model";
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";
import { CreateProfissionalContract } from "../contracts/profissional.contract";
import { ProfissionalService } from "../services/profissional.service";
import { Profissional } from "../entities/profissional.entity";


// localhost:3000/profissional/
@Controller('profissional')
export class ProfissionalController {

    constructor(private readonly service: ProfissionalService){}

    //recebe todos os profissionais e retorna a lista de profissionais
    @Get('get')
    async get() {
        try{
            const profissional = await this.service.get(); 
            return new Result(null, true, profissional, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível listar os profissionais.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }

    @Post('new')
    @UseInterceptors(new ValidatorInterceptor(new CreateProfissionalContract))
    async post(@Body() body: Profissional) {
        try{
            await this.service.post(body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível incluir o profissional.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }
    
    @Put('edit')
    async put(@Param('id') id, @Body() body: Profissional) {
        try{
            await this.service.put(id, body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o profissional.', false, null, error), HttpStatus.BAD_REQUEST)
        } 
    }

    @Delete('delete')
    async delete(@Param('id') id) {
        try{
            await this.service.delete(id); 
            return new Result("Profissional removido com sucesso!", true, null, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o profissional.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }    
}