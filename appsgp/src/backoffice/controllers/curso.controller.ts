import { Controller, Get, Post, Put, Delete, Param, Body, UseInterceptors, HttpException, HttpStatus } from "@nestjs/common";
import { Result } from "../models/result.model";
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";
import { CursoService } from "../services/curso.service";
import { CreateCursoContract } from "../contracts/curso.contract";
import { Curso } from "../entities/curso.entity";

// localhost:3000/curso
@Controller('curso')
export class CursoController {

    constructor(private readonly service: CursoService){}

    //recebe todos as cursos e retorna a lista de cursos
    //localhost:3000/curso/get
    @Get('get')
    async get() {
        try{
            const curso = await this.service.get(); 
            return new Result(null, true, curso, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível listar os cursos.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }
    //localhost:3000/curso/create
    @Post('new')
    @UseInterceptors(new ValidatorInterceptor(new CreateCursoContract))
    async post(@Body() body: Curso) {
        try{
            await this.service.post(body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível incluir o curso.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }
    
    @Put('edit')
    async put(@Param('id') id, @Body() body: Curso) {
        try{
            await this.service.put(id, body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o curso.', false, null, error), HttpStatus.BAD_REQUEST)
        } 
    }

    //o curso será inativado
   /* @Delete(':id/delete')
    async delete(@Param('id') id) {
        try{
            await this.service.delete(id); 
            return new Result("Curso removido com sucesso!", true, null, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o curso.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }    */
}