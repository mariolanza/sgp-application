import { Controller, Get, Post, Put, Delete, Param, Body, UseInterceptors, HttpException, HttpStatus } from "@nestjs/common";
import { Result } from "../models/result.model";
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";
import { PagamentoService } from "../services/pagamento.service";
import { CreatePagamentoContract } from "../contracts/pagamento.contract";
import { Pagamento } from "../entities/pagamento.entity";

// localhost:3000/pagamento
@Controller('pagamento')
export class PagamentoController {

    constructor(private readonly service: PagamentoService){}

    //recebe todos as pagamentos e retorna a lista de pagamentos
    @Get('get')
    async get() {
        try{
            const pagamentos = await this.service.get(); 
            return new Result(null, true, pagamentos, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível listar os pagamentos.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }

    @Post('new')
    @UseInterceptors(new ValidatorInterceptor(new CreatePagamentoContract))
    async post(@Body() body: Pagamento) {
        //se for um coordenador, deve ser atribuido o valor da parcela e somar +1
        //comparar a quantidade de aulas com o limite de aulas por mês
        try{
            await this.service.post(body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível incluir o pagamento.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }
    
    /*@Put('edit')
    async put(@Param('id') id, @Body() body: Pagamento) {
        try{
            await this.service.put(id, body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o pagamento.', false, null, error), HttpStatus.BAD_REQUEST)
        } 
    }*/

    //pagamento não deverá ser deletado
   /* @Delete(':id/delete')
    async delete(@Param('id') id) {
        try{
            await this.service.delete(id); 
            return new Result("Pagamento removido com sucesso!", true, null, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar o pagamento.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }    */
}