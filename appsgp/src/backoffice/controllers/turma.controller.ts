import { Controller, Get, Post, Put, Delete, Param, Body, UseInterceptors, HttpException, HttpStatus } from "@nestjs/common";
import { Result } from "../models/result.model";
import { ValidatorInterceptor } from "src/interceptors/validator.interceptor";
import { TurmaService } from "../services/turma.service";
import { CreateTurmaContract } from "../contracts/turma.contract";
import { Turma } from "../entities/turma.entity";
import { CursoService } from "../services/curso.service";

// localhost:3000/turma
@Controller('turma')
export class TurmaController {

    constructor(private readonly service: TurmaService,
                private readonly serviceCurso: CursoService
        ){}

    //recebe todos as turmas e retorna a lista de turmas
    @Get('get')
    async get() {
        try{
            const turmas = await this.service.get(); 
            return new Result(null, true, turmas, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível listar as turmas.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }

    @Post('new')
    @UseInterceptors(new ValidatorInterceptor(new CreateTurmaContract))
    async post(@Body() body: Turma) {
        try{
            
            
            await this.service.post(body);
            

            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível incluir a turma.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }
    
    @Put('edit')
    async put(@Param('id') id, @Body() body: Turma) {
        try{
            await this.service.put(id, body); 
            return new Result(null, true, body, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar a turma.', false, null, error), HttpStatus.BAD_REQUEST)
        } 
    }

    @Delete('delete')
    async delete(@Param('id') id) {
        try{
            await this.service.delete(id); 
            return new Result("Turma removida com sucesso!", true, null, null);
        } catch (error) {
            throw new HttpException(new Result('Não foi possível atualizar a turma.', false, null, error), HttpStatus.BAD_REQUEST)
        }
    }    
}