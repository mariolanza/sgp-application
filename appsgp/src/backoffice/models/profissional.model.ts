import { Account } from "./account.model";

export class Profissional{

    constructor(

        public nome: string,
        public matricula: string,
        public declaração: boolean,
        public rg: string,
        public orgao_expedidor: string,
        public uf: string,
        public cpf: string,
        public pis: string,
        public banco: string,
        public agencia: string,
        public conta: string,
        public endereco: string,
        public bairro: string,
        public cidade: string,
        public estado: string,
        public cep: string,
        public telefone: string,
        public data_nascimento: Date,
        public cargo: string,
        public titulacao: number,

    ) {}
}