import { Curso } from "./curso.model";

export class Turma{

    constructor(

        public codigo: string,
        public curso: Curso
    ){}
}
