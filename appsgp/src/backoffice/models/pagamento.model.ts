import { Profissional } from "./profissional.model";
import { Curso } from "./curso.model";
import { Disciplina } from "./disciplina.model";

export class Pagamento{

    constructor(

        public data: Date,
        public valor: number,
        public periodo: string,
        public horas_em_sala: number,
        public profissional: Profissional, 
        public curso: Curso,
        public disciplina: Disciplina,
        public valor_total: number,

    ){}
}

