import { Turma } from "../entities/turma.entity";

export class Curso{

    constructor(

        public descricao: string,
        public data_inicio: Date,
        public ultima_parcela_paga_coordenador: number, 
        public ativo: boolean,
        public turma: Turma[]
         
    ){}
}