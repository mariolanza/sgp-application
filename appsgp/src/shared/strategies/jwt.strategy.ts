import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { AuthService } from "../services/auth.service";
import { ExtractJwt, Strategy } from "passport-jwt";
import { JwtPayload } from "../interfaces/jwt-payload.interface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({jwtFromRequest:  ExtractJwt.fromAuthHeaderAsBearerToken(),
                secretOrKey: '1abc2def',
        });
    }

    async validate(payload: JwtPayload) {
        const login = await this.authService.validateUser(payload);
        if(!login){
            throw new UnauthorizedException();  

        }
        return login;
    }
}