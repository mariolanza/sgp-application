import { Injectable, ExecutionContext, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

//método criando pra testar se a rota pode ser ativada ou não
@Injectable()
export class JwtAuthGuard extends  AuthGuard(){
    canActivate(context: ExecutionContext) {
        return super.canActivate(context);
    }

    handleRequest(err, login, info){
        if(err|| !login) {
            throw err || new UnauthorizedException();
        }
        return login;
    }
}