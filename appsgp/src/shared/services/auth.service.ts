import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { AccountService } from "src/backoffice/services/account.service";
import { JwtPayload } from "../interfaces/jwt-payload.interface";


@Injectable()
export class AuthService {
    constructor(
        //private readonly accountService: AccountService,
        private readonly jwtService: JwtService,
    ){}

    //criando o token apenas com a informação de login
    async createToken(login) {
        const payload: JwtPayload = {
            login: login
        };
        return this.jwtService.sign(payload);  
    }

    async validateUser(payload: JwtPayload): Promise<any> {
       return payload;
        // return await this.accountService.findOneByLogin(payload.login);
    }
}