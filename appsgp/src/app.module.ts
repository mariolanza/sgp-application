import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BackofficeModule } from './backoffice/backoffice.module';
import { ConnectionModule } from './backoffice/models/connection/connection.module';

//para finalizar a exe do node => taskkill /F /IM node.exe
//typeorm abstrai o sql, cria as queries no formato esperado pelo tipo de banco utilizado. 
@Module({
  imports: [BackofficeModule, ConnectionModule, 
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'node',
      password: '12345678',
      database: 'sgpapp', 
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true
      })],
  controllers: [],
  providers: [],
})
export class AppModule {}
