import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidator } from 'src/app/validators/custom.validators';
import { Security } from 'src/app/utils/security.util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  public form: FormGroup;
  public busy = false;

  constructor(
    private router: Router,
    private service: DataService,
    private fb: FormBuilder
  ) {

      this.form = this.fb.group({
        login: ['', Validators.required ],
        senha: ['', Validators.compose([
          Validators.minLength(6),
          Validators.maxLength(20),
          Validators.required
        ])]
      });
    }

  ngOnInit() {
    const token = localStorage.getItem("sgp.token");
    if (token) {
      this.busy = true;
      this.service.refreshToken().subscribe(
        (data: any) => {
          this.busy = false;
        },
        (err) => {
          localStorage.clear();
          this.busy = false;
        }
      )
    }
  }

  submit(){
      this.busy = true;
      this.service.authenticate(this.form.value).subscribe(
        (data: any) => {
          console.log(data);
          this.busy = false;
      },
      (err) => {
        console.log(err);
        this.busy = false;
      }
    );
  }

  setUser(user, token) {
    Security.set(user, token);
    this.router.navigate(['/']);
  }

}
