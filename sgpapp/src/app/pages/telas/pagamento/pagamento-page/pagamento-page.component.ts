import { Component, OnInit } from '@angular/core';
import { PagamentoService } from 'src/app/services/pagamento.service';
import { Pagamento } from 'src/app/models/Pagamento.model';

@Component({
  selector: 'app-pagamento-page',
  templateUrl: './pagamento-page.component.html',
  styleUrls: ['./pagamento-page.component.css']
})
export class PagamentoPageComponent implements OnInit {

  pagamento$: Pagamento[] =[];

  constructor(private service: PagamentoService) { }

  ngOnInit() {
    this.service.getPagamentos().subscribe(x => {
      this.pagamento$ = x;
    });
  }

  /*delete(pagamento){
    const mustDelete = confirm("Deseja mesmo deletar pagamento?");

    if(mustDelete){
      this.service
        .deletePagamento(pagamento)
        .subscribe(
          () => (
            (this.pagamento$ = this.pagamento$.filter(x => x !== pagamento)),
            () => alert("Erro ao deletar!")
          )
        );
    }

  }*/

}
