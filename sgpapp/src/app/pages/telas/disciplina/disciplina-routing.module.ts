import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DisciplinaPageComponent } from './disciplina-page/disciplina-page.component';
import { DisciplinaPageFormComponent } from './disciplina-page-form/disciplina-page-form.component';


const routes: Routes = [
  { path: "", component: DisciplinaPageComponent },
  { path: "new", component: DisciplinaPageFormComponent },
  { path: "edit", component: DisciplinaPageFormComponent },
  { path: "get", component: DisciplinaPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisciplinasRoutingModule {}