import { Component, OnInit } from '@angular/core';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { Disciplina } from 'src/app/models/disciplina.model';

@Component({
  selector: 'app-disciplina-page',
  templateUrl: './disciplina-page.component.html',
  styleUrls: ['./disciplina-page.component.css']
})
export class DisciplinaPageComponent implements OnInit {

  disciplina$: Disciplina[] =[];

  constructor(private service: DisciplinaService) { }

  ngOnInit() {
    this.service.getDisciplinas().subscribe(x => {
      this.disciplina$ = x;
    });
  }

  delete(Disciplina){
    const mustDelete = confirm("Deseja mesmo deletar Disciplina?");

    if(mustDelete){
      this.service
        .deleteDisciplina(Disciplina)
        .subscribe(
          () => (
            (this.disciplina$ = this.disciplina$.filter(x => x !== Disciplina)),
            () => alert("Erro ao deletar!")
          )
        );
    }

  }

}
