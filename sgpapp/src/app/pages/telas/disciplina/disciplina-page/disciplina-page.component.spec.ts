import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisciplinaPageComponent } from './disciplina-page.component';

describe('DisciplinaPageComponent', () => {
  let component: DisciplinaPageComponent;
  let fixture: ComponentFixture<DisciplinaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisciplinaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisciplinaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
