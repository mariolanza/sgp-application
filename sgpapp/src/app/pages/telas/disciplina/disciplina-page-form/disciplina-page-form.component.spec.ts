import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisciplinaPageFormComponent } from './disciplina-page-form.component';

describe('DisciplinaPageFormComponent', () => {
  let component: DisciplinaPageFormComponent;
  let fixture: ComponentFixture<DisciplinaPageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisciplinaPageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisciplinaPageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
