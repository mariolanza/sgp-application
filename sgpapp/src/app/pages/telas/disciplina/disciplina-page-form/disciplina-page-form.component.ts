import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { Disciplina } from 'src/app/models/disciplina.model';

@Component({
  selector: 'app-disciplina-page-form',
  templateUrl: './disciplina-page-form.component.html',
  styleUrls: ['./disciplina-page-form.component.css']
})
export class DisciplinaPageFormComponent implements OnInit, AfterContentChecked {

  currentAction: string;
  disciplinaForm: FormGroup;
  pageTitle: string;
  serverErrorMessage: string[] = null;
  submittingForm: boolean;
  disciplina$: Disciplina;

  constructor(
    private service: DisciplinaService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.setCurrentAction();
    this.buildDisciplinaForm();
    this.loadDisciplina();
    this.disciplina$ = new Disciplina();
  }

  ngAfterContentChecked(): void{
    this.setPageTitle();
  }

  submitForm(){
    this.submittingForm = true;
    if(this.currentAction === "new"){
      this.createDisciplina();
    }else{
      this.updateDisciplina();
    }
  }

  private createDisciplina(){
    const disciplina: Disciplina = Object.assign(
      new Disciplina(),
      this.disciplinaForm.value
    );

    this.service
      .createDisciplina(disciplina)
      .subscribe(
        x => this.actionForSuccess(x),
        error => this.actionForError(error)
      );
  }

  private actionForSuccess(disciplina: Disciplina){
    this.router
      .navigateByUrl("disciplina", {skipLocationChange: true})
      .then(() => this.router.navigate(["disciplina", disciplina._id, "edit"]))
    this.toastr.success("Solicitação processada com sucesso!");  
  }

  private actionForError(error){
    this.toastr.error("Ocorreu um erro na solicitação");
    this.submittingForm = false;
    if(error.status === 422){
      this.serverErrorMessage = JSON.parse(error._body).errors;
    }else{
      this.serverErrorMessage = ["Falha na comunicacao com o servidor! Tente mais tarde."];
    }
  }

  private updateDisciplina(){
    const disciplina: Disciplina = Object.assign(
      new Disciplina(),
      this.disciplinaForm.value
    );
    this.service.updateDisciplina(disciplina).subscribe(
      x => this.actionForSuccess(x),
      error => this.actionForError(error)
    );

  }

  private setPageTitle() {
    if(this.currentAction === "new"){
      this.pageTitle = "Cadastro de nova Disciplina";
    }else{
      const disciplinaName = this.disciplina$.descricao || "";
      this.pageTitle = "Editando disciplina: " + disciplinaName;
    }
  }

  private setCurrentAction(){
    if(this.route.snapshot.url[0].path === "new"){
      this.currentAction = "new";
    }else{
      this.currentAction = "edit";
    }
  }

  private buildDisciplinaForm(){
    this.disciplinaForm = this.formBuilder.group({
      _id: [null],
      descricao: [null, [Validators.required, Validators.minLength(2)]],
      carga_horaria_disciplina: [null, [Validators.required]],
    });
  }

  private loadDisciplina(){
    if(this.currentAction === "edit"){
        this.route.paramMap
          .pipe(
            switchMap(params => this.service.getDisciplinaId(+params.get("id")))
          )
          .subscribe(
            disciplina => {
              this.disciplina$ = disciplina;
              this.disciplinaForm.patchValue(this.disciplina$);
            },
            error => this.toastr.error("Ocorreu um erro: "+ error)
          );
    }
  }

}
