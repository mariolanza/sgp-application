import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TurmaPageComponent } from './turma-page/turma-page.component';
import { TurmaPageFormComponent } from './turma-page-form/turma-page-form.component';


const routes: Routes = [
  { path: "", component: TurmaPageComponent },
  { path: "new", component: TurmaPageFormComponent },
  { path: "edit", component: TurmaPageFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TurmasRoutingModule {}