import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Turma } from 'src/app/models/Turma.model';
import { TurmaService } from 'src/app/services/turma.service';
import { Curso } from 'src/app/models/Curso.model';
import { CursoService } from 'src/app/services/curso.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-turma-page-form',
  templateUrl: './turma-page-form.component.html',
  styleUrls: ['./turma-page-form.component.css']
})
export class TurmaPageFormComponent implements OnInit, AfterContentChecked {
  currentAction: string;
  turmaForm: FormGroup;
  pageTitle: string;
  serverErrorMessage: string[] = null;
  submittingForm: boolean;
  turma$: Turma;
  cursos: Array<Curso>;

  constructor(
    private service: TurmaService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private cursoService: CursoService
  ) { }

  ngOnInit() {
    this.setCurrentAction();
    this.buildTurmaForm();
    this.loadCurso();
    this.loadTurmas();
    this.turma$ = new Turma();
  }

  private loadTurmas(){
    this.cursoService.getCursos().subscribe(x => {
      this.cursos = x;
    });
  }

  ngAfterContentChecked(): void {
    this.setPageTitle();
  }

  submitForm(){
    this.submittingForm = true;
    if(this.currentAction === "new"){
      this.createTurma();
    }else{
      this.updateTurma();
    }
  }

  private createTurma(){
    const turma: Turma = Object.assign(new Turma(), this.turmaForm.value);

    this.service
      .createTurma(turma)
      .subscribe(
        x => this.actionForSuccess(x),
        error => this.actionForError(error)
      );
  }

  private actionForSuccess(turma: Turma){
    this.router
      .navigateByUrl("turmas", {skipLocationChange: true})
      .then(() => this.router.navigate(["cursos", turma._id]));

    this.toastr.success("Solicitação processada com sucesso!");  
  }

  private actionForError(error){
    this.toastr.error("Ocorreu um erro na solicitação!");
    this.submittingForm = false;
    if(error.status === 422){
      this.serverErrorMessage = JSON.parse(error._body).errors;
    }else{
      this.serverErrorMessage = [
        "Falha na comunicação com o servidor. Tente mais tarde!"
      ];
    }
  }

  private updateTurma(){
    const turma: Turma = Object.assign(new Turma(), this.turmaForm.value);
    this.service
      .updateTurma(turma)
      .subscribe(
        x => this.actionForSuccess(x),
        error => this.actionForError(error)
      );
  }

  private setPageTitle(){
    if(this.currentAction === "new") {
      this.pageTitle = "Cadastro de novo Turma";
    }else{
      const turmaName = this.turma$.codigo || "";
      this.pageTitle = "Editando Turma: " + turmaName;
    }
  }

  private setCurrentAction(){
    if(this.route.snapshot.url[0].path === "new"){
      this.currentAction = "new";
    }else{
      this.currentAction = "edit";
    }
  }


  private buildTurmaForm(){
    this.turmaForm = this.formBuilder.group({
      _id: [null],
      codigo: [null, [Validators.required, Validators.minLength(1)]],
      cursoId: [null, [Validators.required]],
      curso: [null, [Validators.required]]
    });
  }

  private loadCurso(){
    if(this.currentAction === 'edit'){
      this.route.paramMap
        .pipe(switchMap(params => this.service.getTurmaId(+params.get("id"))))
        .subscribe(
          turma => {
            this.turma$ = turma;
            this.turmaForm.patchValue(this.turma$);
          },
          error => this.toastr.error("Ocorreu um erro: " + error)
        );
    }
  }
}
