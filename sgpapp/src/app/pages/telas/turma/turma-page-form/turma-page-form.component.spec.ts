import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurmaPageFormComponent } from './turma-page-form.component';

describe('TurmaPageFormComponent', () => {
  let component: TurmaPageFormComponent;
  let fixture: ComponentFixture<TurmaPageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurmaPageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurmaPageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
