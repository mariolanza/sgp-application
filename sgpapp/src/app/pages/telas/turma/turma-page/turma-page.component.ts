import { Component, OnInit } from '@angular/core';
import { TurmaService } from 'src/app/services/turma.service';
import { Turma } from 'src/app/models/Turma.model';

@Component({
  selector: 'app-turma-page',
  templateUrl: './turma-page.component.html',
  styleUrls: ['./turma-page.component.css']
})
export class TurmaPageComponent implements OnInit {

  turma$: Turma[] =[];

  constructor(private service: TurmaService) { }

  ngOnInit() {
    this.service.getTurmas().subscribe(x => {

      this.turma$ = x;
    });
  }

  handleClick(){
    
  }

  delete(turma){
    const mustDelete = confirm("Deseja mesmo deletar turma?");

    if(mustDelete){
      this.service
        .deleteTurma(turma)
        .subscribe(
          () => (
            (this.turma$ = this.turma$.filter(x => x !== turma)),
            () => alert("Erro ao deletar!")
          )
        );
    }

  }

}

