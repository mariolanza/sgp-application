import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurmaPageComponent } from './turma-page.component';

describe('TurmaPageComponent', () => {
  let component: TurmaPageComponent;
  let fixture: ComponentFixture<TurmaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurmaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurmaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
