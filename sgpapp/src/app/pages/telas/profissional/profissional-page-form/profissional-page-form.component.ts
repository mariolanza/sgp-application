import { Component, OnInit, AfterContentChecked } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { Profissional } from 'src/app/models/profissional.model';
import { ProfissionalService } from 'src/app/services/profissional.service';

@Component({
  selector: 'app-profissional-page-form',
  templateUrl: './profissional-page-form.component.html',
  styleUrls: ['./profissional-page-form.component.css']
})
export class ProfissionalPageFormComponent implements OnInit, AfterContentChecked {

  currentAction: string;
  profissionalForm: FormGroup;
  pageTitle: string;
  serverErrorMessage: string[] = null;
  submittingForm: boolean;
  profissional$: Profissional;

  constructor(
    private service: ProfissionalService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.setCurrentAction();
    this.buildProfissionalForm();
    this.loadProfissional();
    this.profissional$ = new Profissional();
  }

  ngAfterContentChecked(): void {
    this.setPageTitle();
  }

  submitForm() {
    this.submittingForm = true;
    if (this.currentAction === "new") {
      this.createProfissional();
    } else {
      this.updateProfissional();
    }
  }

  private createProfissional() {
    const profissional: Profissional = Object.assign(
      new Profissional(),
      this.profissionalForm.value
    );

    this.service
      .createProfissional(profissional)
      .subscribe(
        x => this.actionForSuccess(x),
        error => this.actionForError(error)
      );
  }

  private actionForSuccess(profissional: Profissional) {
    this.router
      .navigateByUrl("profissional", { skipLocationChange: true })
      .then(() => this.router.navigate(["profissional", profissional._id, "edit"]))
    this.toastr.success("Solicitação processada com sucesso!");
  }

  private actionForError(error) {
    this.toastr.error("Ocorreu um erro na solicitação");
    this.submittingForm = false;
    if (error.status === 422) {
      this.serverErrorMessage = JSON.parse(error._body).errors;
    } else {
      this.serverErrorMessage = ["Falha na comunicacao com o servidor! Tente mais tarde."];
    }
  }

  private updateProfissional() {
    const profissional: Profissional = Object.assign(
      new Profissional(),
      this.profissionalForm.value
    );
    this.service.updateProfissional(profissional).subscribe(
      x => this.actionForSuccess(x),
      error => this.actionForError(error)
    );

  }

  private setPageTitle() {
    if (this.currentAction === "new") {
      this.pageTitle = "Cadastro de novo Profissional";
    } else {
      const profissionalName = this.profissional$.nome || "";
      this.pageTitle = "Editando profisisonal: " + profissionalName;
    }
  }

  private setCurrentAction() {
    if (this.route.snapshot.url[0].path === "new") {
      this.currentAction = "new";
    } else {
      this.currentAction = "edit";
    }
  }

  private buildProfissionalForm() {
    this.profissionalForm = this.formBuilder.group({
      _id: [null],
      nome: [null, [Validators.required, Validators.minLength(2)]],
      declaracao: [null, [Validators.required, Validators.requiredTrue]],
      rg: [null, [Validators.required, Validators.minLength(8)]],
      orgao_expedidor: [null, [Validators.required, Validators.minLength(3)]],
      uf: [null, [Validators.required, Validators.minLength(2)]],
      cpf: [null, [Validators.required, Validators.minLength(11)]],
      pis: [null, [Validators.required, Validators.minLength(11)]],
      banco: [null, [Validators.required, Validators.minLength(2)]],
      agencia: [null, [Validators.required, Validators.minLength(4)]],
      conta: [null, [Validators.required, Validators.minLength(5)]],
      endereco: [null, [Validators.required, Validators.minLength(2)]],
      bairro: [null, [Validators.required, Validators.minLength(2)]],
      cidade: [null, [Validators.required, Validators.minLength(2)]],
      estado: [null, [Validators.required, Validators.minLength(2)]],
      cep: [null, [Validators.required, Validators.minLength(8)]],
      telefone: [null, [Validators.required, Validators.minLength(2)]],
      data_nascimento: [null, [Validators.required, Validators.minLength(8)]],
      cargo: [null, [Validators.required, Validators.minLength(2)]],
    });
  }

  private loadProfissional() {
    if (this.currentAction === "edit") {
      this.route.paramMap
        .pipe(
          switchMap(params => this.service.getProfissionalId(+params.get("id")))
        )
        .subscribe(
          profissional => {
            this.profissional$ = profissional;
            this.profissionalForm.patchValue(this.profissional$);
          },
          error => this.toastr.error("Ocorreu um erro: " + error)
        );
    }
  }

}
