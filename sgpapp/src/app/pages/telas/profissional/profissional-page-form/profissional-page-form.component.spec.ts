import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfissionalPageFormComponent } from './profissional-page-form.component';

describe('ProfissionalPageFormComponent', () => {
  let component: ProfissionalPageFormComponent;
  let fixture: ComponentFixture<ProfissionalPageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfissionalPageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfissionalPageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
