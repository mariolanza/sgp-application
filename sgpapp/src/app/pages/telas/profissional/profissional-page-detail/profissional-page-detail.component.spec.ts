import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfissionalPageDetailComponent } from './profissional-page-detail.component';

describe('ProfissionalPageDetailComponent', () => {
  let component: ProfissionalPageDetailComponent;
  let fixture: ComponentFixture<ProfissionalPageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfissionalPageDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfissionalPageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
