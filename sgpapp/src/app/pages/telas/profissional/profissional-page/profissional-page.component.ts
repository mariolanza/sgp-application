import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfissionalService } from 'src/app/services/profissional.service';
import { Profissional } from 'src/app/models/profissional.model';


@Component({
  selector: 'app-profissional-page',
  templateUrl: './profissional-page.component.html',
  styleUrls: ['./profissional-page.component.css']
})
export class ProfissionalPageComponent implements OnInit {
  public profissional$: Profissional[] = [];

  constructor(private service: ProfissionalService) { }

  ngOnInit() {
    this.service.getProfissionais().subscribe(x => {
      this.profissional$ = x;
    });
  }
  delete(profissional) {
    const mustDelete = confirm("Deseja realmente deletar o profissional?");

    if (mustDelete) {
      this.service.deleteProfissional(profissional)
        .subscribe(
          () => (
            (this.profissional$ = this.profissional$.filter(x => x !== profissional)),
            () => alert("Erro ao deletar!")
          )
        )
    }
  }

}
