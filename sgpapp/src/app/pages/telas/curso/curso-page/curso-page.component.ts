import { Component, OnInit } from '@angular/core';
import { CursoService } from 'src/app/services/curso.service';
import { Curso } from 'src/app/models/Curso.model';

@Component({
  selector: 'app-curso-page',
  templateUrl: './curso-page.component.html',
  styleUrls: ['./curso-page.component.css']
})
export class CursoPageComponent implements OnInit {

  curso$: Curso[] =[];

  constructor(private service: CursoService) { }

  ngOnInit() {
    this.service.getCursos().subscribe(x => {
      this.curso$ = x;
    });
  }

  delete(curso){
    const mustDelete = confirm("Deseja mesmo deletar curso?");

    if(mustDelete){
      this.service
        .deleteCurso(curso)
        .subscribe(
          () => (
            (this.curso$ = this.curso$.filter(x => x !== curso)),
            () => alert("Erro ao deletar!")
          )
        );
    }

  }

}
