import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CursoPageFormComponent } from './curso-page-form.component';

describe('CursoPageFormComponent', () => {
  let component: CursoPageFormComponent;
  let fixture: ComponentFixture<CursoPageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CursoPageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CursoPageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
