import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Turma } from 'src/app/models/Turma.model';
import { TurmaService } from 'src/app/services/turma.service';
import { Curso } from 'src/app/models/Curso.model';
import { CursoService } from 'src/app/services/curso.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-curso-page-form',
  templateUrl: './curso-page-form.component.html',
  styleUrls: ['./curso-page-form.component.css']
})
export class CursoPageFormComponent implements OnInit, AfterContentChecked {
  currentAction: string
  cursoForm: FormGroup;
  pageTitle: string;
  serverErrorMessage: string[] = null;
  submittingForm: boolean;
  curso$: Curso;
  turmas: Array<Turma>;

  constructor(
    private service: CursoService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private turmaService: TurmaService
  ) { }

  ngOnInit() {
    this.setCurrentAction();
    this.buildCursoForm();
    this.loadCurso();
    this.loadTurmas();
    this.curso$ = new Curso();
  }

  private loadTurmas(){
    this.turmaService.getTurmas().subscribe(x => {
      this.turmas = x;
    });
  }

  ngAfterContentChecked(): void {
    this.setPageTitle();
  }

  submitForm(){
    this.submittingForm = true;
    if(this.currentAction === "new"){
      this.createCurso();
    }else{
      this.updateCurso();
    }
  }

  private createCurso(){
    const curso: Curso = Object.assign(new Curso(), this.cursoForm.value);

    this.service
      .createCurso(curso)
      .subscribe(
        x => this.actionForSuccess(x),
        error => this.actionForError(error)
      );
  }

  private actionForSuccess(curso: Curso){
    this.router
      .navigateByUrl("cursos", {skipLocationChange: true})
      .then(() => this.router.navigate(["cursos", curso._id]));

    this.toastr.success("Solicitação processada com sucesso!");  
  }

  private actionForError(error){
    this.toastr.error("Ocorreu um erro na solicitação!");
    this.submittingForm = false;
    if(error.status === 422){
      this.serverErrorMessage = JSON.parse(error._body).errors;
    }else{
      this.serverErrorMessage = [
        "Falha na comunicação com o servidor. Tente mais tarde!"
      ];
    }
  }

  private updateCurso(){
    const curso: Curso = Object.assign(new Curso(), this.cursoForm.value);
    this.service
      .updateCurso(curso)
      .subscribe(
        x => this.actionForSuccess(x),
        error => this.actionForError(error)
      );
  }

  private setPageTitle(){
    if(this.currentAction === "new") {
      this.pageTitle = "Cadastro de novo Curso";
    }else{
      const cursoName = this.curso$.descricao || "";
      this.pageTitle = "Editando Curso: " + cursoName;
    }
  }

  private setCurrentAction(){
    if(this.route.snapshot.url[0].path === "new"){
      this.currentAction = "new";
    }else{
      this.currentAction = "edit";
    }
  }

  private buildCursoForm(){
    this.cursoForm = this.formBuilder.group({
      _id: [null],
      descrecao: [null, [Validators.required, Validators.minLength(2)]],
      data_inicio: [null, [Validators.required]],
      ultima_parcela_paga_coordenador: [null, [Validators.required]],
      ativo: [null],
      turmaId: [null, [Validators.required]]
    });
  }

  private loadCurso(){
    if(this.currentAction === 'edit'){
      this.route.paramMap
        .pipe(switchMap(params => this.service.getCursoId(+params.get("id"))))
        .subscribe(
          curso => {
            this.curso$ = curso;
            this.cursoForm.patchValue(this.curso$);
          },
          error => this.toastr.error("Ocorreu um erro: " + error)
        );
    }
  }
}
