import { Account } from "../models/Account.model";

export class Security {
    public static set(user: Account, token: string) {
        const data = JSON.stringify(user);

        localStorage.setItem('sgpuser', btoa(data));
        localStorage.setItem('sgptoken', token);
    }

    public static setUser(user: Account) {
        const data = JSON.stringify(user);
        localStorage.setItem('sgpuser', btoa(data));
    }

    public static setToken(token: string) {
        localStorage.setItem('sgptoken', token);
    }

    public static getUser(): Account {
        const data = localStorage.getItem('sgpuser');
        if (data) {
            return JSON.parse(atob(data));
        } else {
            return null;
        }
    }

    public static getToken(): string {
        const data = localStorage.getItem('sgptoken');
        if (data) {
            return data;
        } else {
            return null;
        }
    }

    public static hasToken(): boolean {
        if (this.getToken())
            return true;
        else
            return false;
    }

    public static clear() {
        localStorage.removeItem('sgpuser');
        localStorage.removeItem('sgptoken');
    }
}