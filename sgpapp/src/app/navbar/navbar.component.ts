import { Component, OnInit } from '@angular/core';
import { Account } from '../models/Account.model';
import { Security } from '../utils/security.util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public user: Account;

  constructor(private router: Router) { }

  ngOnInit() {
    this.user = Security.getUser();
  }

  logout(){
    Security.clear();
    this.router.navigate(['/login']);
  }
}
