import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './pages/account/login-page/login-page.component';
import { FramePageComponent } from './pages/master/frame.page';
import { ProfissionalPageComponent } from './pages/telas/profissional/profissional-page/profissional-page.component';
import { PagamentoPageComponent } from './pages/telas/pagamento/pagamento-page/pagamento-page.component';
import { SignupPageComponent } from './pages/account/signup-page/signup-page.component';
import { ResetPasswordPageComponent } from './pages/account/reset-password-page/reset-password-page.component';
import { DisciplinaPageComponent } from './pages/telas/disciplina/disciplina-page/disciplina-page.component';
import { CursoPageComponent } from './pages/telas/curso/curso-page/curso-page.component';
import { RelatorioPageComponent } from './pages/telas/relatorio/relatorio-page/relatorio-page.component';
import { TurmaPageComponent } from './pages/telas/turma/turma-page/turma-page.component';
import { DisciplinaPageFormComponent } from './pages/telas/disciplina/disciplina-page-form/disciplina-page-form.component';
import { ProfissionalPageFormComponent } from './pages/telas/profissional/profissional-page-form/profissional-page-form.component';
import { TurmaPageFormComponent } from './pages/telas/turma/turma-page-form/turma-page-form.component';


const routes: Routes = [
  
  { 
    path: '', 
    component: FramePageComponent,
    children: [
      {path:'', component: ProfissionalPageComponent},
      { path : 'account/turma/new', component: TurmaPageFormComponent},
      { path : 'account/turma/edit', component: TurmaPageFormComponent},
      { path : 'account/disciplina/new', component: DisciplinaPageFormComponent},
      { path : 'account/disciplina/edit', component: DisciplinaPageFormComponent},
      { path : 'account/disciplina/get', component: DisciplinaPageComponent},
      {path: 'new', component: ProfissionalPageFormComponent},
      {path:'relatorio', component: RelatorioPageComponent}
    ]
  },

  /*{ 
    path: 'disciplina', 
    component: FramePageComponent,
    children: [{path:'editar', component: DisciplinaPageFormComponent}]
  },*/

  /*path: 'turma',
  component: TurmaPageComponent
  Esta rota, servirá para quando clicar em turma, no NavBar, chamar a página turma.
  ficaria: localhost:4200/turma
  */
  
  {
    path: 'account',
    component: FramePageComponent,
    children: [      
      {path:'disciplina', component: DisciplinaPageComponent},
      //{path:'profissional', component: ProfissionalPageComponent},
      {path:'turma', component: TurmaPageComponent}, //TODO CRIAR COMPONENTE CORRETAMENTE
      {path:'curso', component: CursoPageComponent},
      {path:'pagamento', component: PagamentoPageComponent}      
    ]
  },

  {path: 'login', component: LoginPageComponent},
  {path:'signup', component: SignupPageComponent},
  {path: 'reset-password', component: ResetPasswordPageComponent},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
