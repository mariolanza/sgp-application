import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pagamento } from '../models/Pagamento.model';
import { Observable, throwError, from } from 'rxjs';
import { map, catchError, flatMap } from 'rxjs/operators';
import { ProfissionalService } from './profissional.service';

@Injectable({
  providedIn: 'root'
})

export class PagamentoService {

  private url = 'http://localhost:3000'

  constructor(private http: HttpClient,
    private profissionalService: ProfissionalService
  ) { }

  getPagamentos() {
    return this.http.get<Pagamento[]>(`${this.url}/pagamento/get`).pipe();
  }

  getPagamentoId(_id: string): Observable<Pagamento> {
    return this.http.get<Pagamento>(`${this.url}/pagamento/${_id}`).pipe(
      catchError(this.handlerError),
      map(this.jsonDataToPagamento)
    );
  }

  createPagamento(pagamento: Pagamento): Observable<Pagamento> {
    return this.profissionalService.getProfissionalId(+pagamento.profissionalId).pipe(
      flatMap(profissional => {
        pagamento.profissional = profissional;
        return this.http.post(`${this.url}/pagamento/new`, pagamento).pipe(
          catchError(this.handlerError),
          map(this.jsonDataToPagamento)
        );
      })
    );
  }

  /*updatePagamento(pagamento: Pagamento): Observable<Pagamento> {
      const url = `${this.url}/pagamento/${pagamento._id}`;
          return this.profissionalService.getProfissionalId(+pagamento.profissionalId).pipe(
            flatMap(profissional => {
              pagamento.profissional = profissional;
              return this.http.put(url, pagamento).pipe(
                catchError(this.handlerError),
                map(this.jsonDataToPagamento)
              )
            })
          )
  }

  deletePagamento(_id: string): Observable<any> {
      return this.http.delete(`${this.url}/pagamento/${_id}`).pipe(
          catchError(this.handlerError),
          map(() => null)
      );
  } */

  private jsonDataToPagamentos(jsonData: any[]): Pagamento[] {
    const Pagamentos: Pagamento[] = [];
    jsonData.forEach(x => Pagamentos.push(x));
    return Pagamentos;
  }

  private handlerError(erro: any): Observable<any> {
    console.log("Erro na requisição", erro);
    return throwError(erro);
  }

  private jsonDataToPagamento(jsonData: any): Pagamento {
    return jsonData as Pagamento;
  }
}
