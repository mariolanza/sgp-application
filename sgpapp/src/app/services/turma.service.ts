import { Injectable } from '@angular/core';
import { HttpClient,  } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { map, catchError, flatMap } from 'rxjs/operators';
import { Turma } from '../models/Turma.model';

@Injectable({
  providedIn: 'root'
})

export class TurmaService {

    private url = 'http://localhost:3000'

    constructor(private http: HttpClient
      ) { }

    getTurmas() {
        return this.http.get<Turma[]>(`${this.url}/turma/get`).pipe(
        );
    }

    getTurmaId(_id: number): Observable<Turma> {
        return this.http.get<Turma>(`${this.url}/turma/${_id}`).pipe(
            catchError(this.handlerError),
            map(this.jsonDataToTurma)
        );
    }

    createTurma(turma: Turma): Observable<Turma> {
        //return this.cursoService.getCursoId(+turma.cursoId).pipe(
          //flatMap(curso => {
            //turma.curso = curso;
            return this.http.post(`${this.url}/turma/new`, turma).pipe(
              catchError(this.handlerError),
              map(this.jsonDataToTurma)
            );
          //})
        //);
    }

    updateTurma(turma: Turma): Observable<Turma> {
        const url = `${this.url}/turma/edit/${turma._id}`;
            //return this.cursoService.getCursoId(+turma.cursoId).pipe(
              //flatMap(curso => {
                //turma.curso = curso;
                return this.http.put(url, turma).pipe(
                  catchError(this.handlerError),
                  map(this.jsonDataToTurma)
                )
          //  })
          //)
    }

    deleteTurma(_id: number): Observable<any> {
        return this.http.delete(`${this.url}/turma/delete/${_id}`).pipe(
            catchError(this.handlerError),
            map(() => null)
        );
    }

    private jsonDataToTurmas(jsonData: any[]): Turma[] {
        const turmas: Turma[] = [];
        jsonData.forEach(x => turmas.push(x));
        return turmas;
    }

    private handlerError(erro: any): Observable<any> {
        console.log("Erro na requisição", erro);
        return throwError(erro);
    }

    private jsonDataToTurma(jsonData: any): Turma {
        return jsonData as Turma;
    }
}
