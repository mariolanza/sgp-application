import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Profissional } from '../models/profissional.model';
import { Account } from '../models/Account.model';


@Injectable({
    providedIn: 'root'
})


export class DataService {

    
    public url = 'http://localhost:3000';

    constructor(private http: HttpClient) { }

    public composeHeaders() {
        const token = localStorage.getItem('sgp.token');
        const headers = new HttpHeaders().set('Authorization', `bearer ${token}`);
        return headers;
    }

    /*getUser() {
        return this.http.get<User[]>(`${this.url}/usuario`);
    }*/

    authenticate(data){
        return this.http.post<Account[]>(`${this.url}/account/authenticate`, data);
    }

    refreshToken(){
        return this.http.post(`${this.url}/account/refresh-token`,
        null,
        {headers: this.composeHeaders()}
        );
    }

    create(data){
        return this.http.post(`${this.url}/account`, data);
    }

    resetPassword(data){
        return this.http.post(`${this.url}/account/reset-password`, data);

    }
}