import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Profissional } from '../models/profissional.model';
import { Observable, throwError } from 'rxjs';
import { map, catchError, flatMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProfissionalService {

    private readonly url = 'http://localhost:3000/profissional'

    constructor(private http: HttpClient) { }

    getProfissionais() {
        return this.http.get<Profissional[]>(`${this.url}/profissional/get`).pipe();
    }

    getProfissionalId(_id: number): Observable<Profissional> {
        return this.http.get<Profissional>(`${this.url}/profissional${_id}`).pipe(
            catchError(this.handlerError),
            map(this.jsonDataToProfissional)
        );
    }

    createProfissional(profissional: Profissional): Observable<Profissional> {
        return this.http.post(`${this.url}/profissional/new`, `${profissional._id}`).pipe(
            catchError(this.handlerError),
            map(this.jsonDataToProfissional)
        );
    }

    updateProfissional(profissional: Profissional) {
        return this.http.put(`${this.url}/profissional/edit`, `${profissional._id}`).pipe(
            catchError(this.handlerError),
            map(() => profissional)
        );
    }

    deleteProfissional(_id: number): Observable<any> {
        return this.http.delete(`${this.url}/profissional/delete/${_id}`).pipe(
            catchError(this.handlerError),
            map(() => null)
        );
    }

    private jsonDataToProfissionais(jsonData: any[]): Profissional[] {
        const profissionais: Profissional[] = [];
        jsonData.forEach(x => profissionais.push(x));
        return profissionais;
    }

    private handlerError(erro: any): Observable<any> {
        console.log("Erro na requisição", erro);
        return throwError(erro);
    }

    private jsonDataToProfissional(jsonData: any): Profissional {
        return jsonData as Profissional;
    }
}