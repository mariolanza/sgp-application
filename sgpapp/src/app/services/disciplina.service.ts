import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Disciplina } from '../models/disciplina.model';
import { Observable, throwError } from 'rxjs';
import { map, catchError, flatMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class DisciplinaService {

    private url = 'http://localhost:3000'

    constructor(private http: HttpClient) { }

    getDisciplinas() {
        return this.http.get<Disciplina[]>(`${this.url}/disciplina/get`).pipe();
    }

    getDisciplinaId(_id: number): Observable<Disciplina> {
        return this.http.get<Disciplina>(`${this.url}/disciplina/${_id}`).pipe(
            catchError(this.handlerError)
        );
    }

    createDisciplina(disciplina: Disciplina): Observable<Disciplina> {
        return this.http.post(`${this.url}/disciplina/new`, disciplina).pipe(
            catchError(this.handlerError)
        );
    }

    updateDisciplina(disciplina: Disciplina) {
        return this.http.put(`${this.url}/disciplina/edit`, `${disciplina._id}`).pipe(
            catchError(this.handlerError)
        );
    }

    deleteDisciplina(_id: number): Observable<any> {
        return this.http.delete(`${this.url}/disciplina/delete/${_id}`).pipe(
            catchError(this.handlerError)
        );
    }

    private handlerError(erro: any): Observable<any> {
        console.log("Erro na requisição", erro);
        return throwError(erro);
    }
}