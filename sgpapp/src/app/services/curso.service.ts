import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Curso } from '../models/Curso.model';
import { Observable, throwError } from 'rxjs';
import { map, catchError, flatMap } from 'rxjs/operators';
import { TurmaService } from './turma.service';

@Injectable({
    providedIn: 'root'
})



// Sistemas de Informação
//     Turma1
//     Turma2


//             Cadastro de Curso

//     Descricao  Sistemas de informação

//                 Turma1
//                     ComboBox(Dis1) ,ComboBox (Professor)  +
           
                      
//                 Turma3


//                 Dis1
//                         CH: 60hrs,
//                         Descricao: 50
export class CursoService {

    private url = 'http://localhost:3000'

    constructor(
        private http: HttpClient,

    ) { }

    getCursos() {
        return this.http.get<Curso[]>(`${this.url}/curso/get`).pipe(
        );
    }

    getCursoId(_id: number): Observable<Curso> {
        return this.http.get(`${this.url}/curso/${_id}`).pipe(
            catchError(this.handlerError),
            map(this.jsonDataToCurso)
        );
    }

    createCurso(curso: Curso): Observable<Curso> {
        //return this.turmaService.getTurmaId(+curso.turmaId).pipe(
            //flatMap(turma => {
                //curso.turma.push(turma);
                return this.http.post(`${this.url}/curso/new`, `${curso._id}`).pipe(
                    catchError(this.handlerError),
                    map(this.jsonDataToCurso)
                );
            //})
        //);
    }

    updateCurso(curso: Curso): Observable<Curso> {
        const url = `${this.url}/curso/edit/${curso._id}`;
        //return this.turmaService.getTurmaId(+curso.turmaId).pipe(
            //flatMap(turma => {
                //curso.turma.push(turma);
                return this.http.put(url, curso).pipe(
                    catchError(this.handlerError),
                    map(this.jsonDataToCurso)
                )
            //})
        //)
    }

    deleteCurso(_id: number): Observable<any> {
        return this.http.delete(`${this.url}/curso/delete${_id}`).pipe(
            catchError(this.handlerError),
            map(() => null)
        );
    }

    private jsonDataToCursos(jsonData: any[]): Curso[] {
        const cursos: Curso[] = [];
        jsonData.forEach(x => {
            const curso = Object.assign(new Curso(), x);
            cursos.push(curso);
        });
        return cursos;
    }

    private handlerError(erro: any): Observable<any> {
        console.log("Erro na requisição", erro);
        return throwError(erro);
    }

    private jsonDataToCurso(jsonData: any): Curso {
        return jsonData as Curso;
    }
}