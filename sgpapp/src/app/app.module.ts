import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TurmaPageComponent } from './pages/telas/turma/turma-page/turma-page.component';
import { LoginPageComponent } from './pages/account/login-page/login-page.component';
import { ResetPasswordPageComponent } from './pages/account/reset-password-page/reset-password-page.component';
import { SignupPageComponent } from './pages/account/signup-page/signup-page.component';
import { ProfissionalPageComponent } from './pages/telas/profissional/profissional-page/profissional-page.component';
import { PagamentoPageComponent } from './pages/telas/pagamento/pagamento-page/pagamento-page.component';
import { CursoPageComponent } from './pages/telas/curso/curso-page/curso-page.component';
import { DisciplinaPageComponent } from './pages/telas/disciplina/disciplina-page/disciplina-page.component';
import { FramePageComponent } from './pages/master/frame.page';
import { RelatorioPageComponent } from './pages/telas/relatorio/relatorio-page/relatorio-page.component';
import { HttpClientModule } from '@angular/common/http';
import { LoadingComponent } from './component/shared/loading/loading.component';
import { ProfissionalPageDetailComponent } from './pages/telas/profissional/profissional-page-detail/profissional-page-detail.component';
import { ProfissionalPageFormComponent } from './pages/telas/profissional/profissional-page-form/profissional-page-form.component';
import { TurmaPageFormComponent } from './pages/telas/turma/turma-page-form/turma-page-form.component';
import { CursoPageFormComponent } from './pages/telas/curso/curso-page-form/curso-page-form.component';
import { DisciplinaPageFormComponent } from './pages/telas/disciplina/disciplina-page-form/disciplina-page-form.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginPageComponent,
    ResetPasswordPageComponent,
    SignupPageComponent, 
    FramePageComponent,
    LoadingComponent,
    CursoPageFormComponent,
    CursoPageComponent,
    PagamentoPageComponent,
    ProfissionalPageDetailComponent,
    ProfissionalPageFormComponent,
    ProfissionalPageComponent,
    TurmaPageFormComponent,
    TurmaPageComponent,
    DisciplinaPageFormComponent,
    DisciplinaPageComponent,
    RelatorioPageComponent,
  ],
  
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
