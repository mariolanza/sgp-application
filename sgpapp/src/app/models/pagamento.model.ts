import { Profissional } from './profissional.model';
import { Curso } from './Curso.model';
import { Disciplina } from './disciplina.model';

export class Pagamento {
        constructor(
                public _id?: number,
                public horas_em_sala?: number,
                public data?: Date,
                public valor?: number,
                public carga_horaria_periodo?: number,
                public periodo?: string,
                public valor_total?: number,
                public profissionalId?: number,
                public profissional?: Profissional,
        ) { }

}
