 import { Turma } from './Turma.model';

export class Curso {
    constructor(
        public _id?: number,
        public descricao?: string,
        public data_inicio?: Date,
        public ultima_parcela_paga_coordenador?: number,
        public ativo?: boolean,
        public turmaId?: string,
        public turma?: Turma[],
    ) { }

}
