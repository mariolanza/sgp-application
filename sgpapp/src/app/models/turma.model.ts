import { Curso } from './Curso.model';

export class Turma {
  constructor(
    public _id?: string,
    public codigo?: string,
    public cursoId?: string,
    public curso?: Curso,
  ) { }

}
