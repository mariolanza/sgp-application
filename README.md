#SGP Application

Este repositório conterá todos o código do projeto em Angular e Node.
Os arquivos anteriores, poderão ser encontrados nos links:
https://bitbucket.org/nu-knupp/tcc-ii-sistema-gerencial-de-pos-graduacao/src/master/
https://bitbucket.org/nu-knupp/sgpapp/src/master/

O link:
https://bitbucket.org/nu-knupp/tcc-ii-sistema-gerencial-de-pos-graduacao/src/master/
Remete-se a documentação do projeto anteriormente upada.

O link:
https://bitbucket.org/nu-knupp/sgpapp/src/master/
Remete-se ao código Angular anteriormente upado.

Os documentos do sistema atualizados, serão agora lançados neste repositório e estarão disponíveis para Download.

Download da documentação do projeto:
https://bitbucket.org/mariolanza/sgp-application/downloads/TCC_Verss%C3%A3o_1.5.2_-_Mario_e_Nubia.docx


#TESTE de commit

#ATUALIZADO PASTA SGPAPP - FRONTEND

04/11/19 - Atualizado as pages (telas) e adicionado as rotas.

#ATUALIZADO PASTA APPSGP - BACKEND
 
06/11/19 - Adicionado "entities" para mapeamento do banco de dados.
            Adicionado "services" para reutilização de código.
            Atualizado "controllers" de acordo com os serviços.

#ATUALIZADO PASTA APPSGP - BACKEND

09/11/19 - Atualizado pasta entity

#ATUALIZADO PASTA APPSGP - BACKEND

10/11/19 - Atualizado pasta de entidades, serviços e controladores 

#ATUALIZADO PASTA SGPAPP - FRONTEND

14/11/2019 - Atualizado validadores, páginas e folhas de estilo

#ATUALIZADO PASTA SGPAPP - FRONTEND

14/11/2019 - Atualizado validadores, utils, login-page e models

#ATUALIZADO PASTA SGPAPP - FRONTEND

15/11/2019 - Atualizado tela de login, barra de navegação, componente profissional, serviços (dados) e profissional HTML

#ATUALIZADO PASTA SGPAPP - FRONTEND

16/11/2019 - Atualizado tela de login, signup, reset password, cadastro de usuário. Inclusos npm modulos (toastrService);
             Atualizado campos de cadastro para profissional.

#ATUALIZADO PASTA APPSGP - BACKEND

17/11/2019 - Criando autenticação de usuário

#ATUALIZADO PASTA APPSGP - BACKEND

17/11/2019 - Update reset and change password, refresh token

#ATUALIZADO PASTA APPSGP - BACKEND

17/11/2019 - Update de todas as pastas, fusão de Pagamento com CursoDisciplinaProfissional
            Faltante, parcela de pagamento do coordenador 

DOCUEMNTO DE TCC ATUALIZADO 
*TCC_Verssão_2.0_-_Mario_e_Nubia_.docx* 
ENCONTRA-SE EM DOWNLOADS

O DOCUMENTO DE TCC ENCONTRA-SE EM: https://bitbucket.org/mariolanza/sgp-application/downloads/